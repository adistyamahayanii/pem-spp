<?php

    class Admin_Transaksi extends Controller
    {
        public function index()
        {
            $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
            $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
            $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/admin/data-transaksi/transaksi', $data);
        }

        public function lihatLaporan()
        {
            $data['transaksi'] = $this->model('Transaksi_model')->allTransaksi();
            $data['bulan'] = [1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember'];
            $data['pilihanData'] = [];
    
            foreach ($data['transaksi'] as $transaksi) {
                $data['pilihanData'][$transaksi['nama'] . '|' . $transaksi['nisn']][] = $transaksi['bulan_dibayar'];
            }
    
            $this->view('home/admin/data-transaksi/laporan', $data);
        }

    }