<?php

    class Admin_Petugas extends Controller
    {
        public function index()
        {
            $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $this->view('home/admin/data-petugas/petugas', $data);
        }
         
        public function prosesTambah()
        {
            // var_dump($_POST);
            // die;
            if($this->model('Petugas_model')->createPetugas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_petugas');
                exit;
            }
        }

        public function edit($id)
        {
            $data['petugas'] = $this->model('Petugas_model')->getPetugasById($id);
            $this->view('home/admin/data-petugas/edit-petugas', $data);
        }

        public function prosesUpdate()
        {
            if($this->model('Petugas_model')->updatePetugas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_petugas');
                exit;
            }
        }

        public function delete($id)
        {
            if($this->model('Petugas_model')->deletePetugas($id) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_petugas');
                exit;
            }
        }
    }