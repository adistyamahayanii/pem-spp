<?php
    class Admin_Siswa extends Controller
    {
        public function index()
        {
            $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
            $data['allKelas'] = $this->model('Kelas_model')->getAllKelas();
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/admin/data-siswa/siswa', $data);
        }

        public function tambahSiswa()
        {
            if($this->model('Siswa_model')->createSiswa($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_siswa');
                exit;
            }
        }

        public function edit($id)
        {
            $data['siswa'] = $this->model('Siswa_model')->getSiswaById($id);
            $this->view('home/admin/data-siswa/edit-siswa', $data);
        }

        public function prosesUpdate()
        {
            if($this->model('Siswa_model')->updateSiswa($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_siswa');
                exit;
            }
        }

        public function delete($id)
        {
            if($this->model('Siswa_model')->deleteSiswa($id) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_siswa');
                exit;
            }
        }
    }