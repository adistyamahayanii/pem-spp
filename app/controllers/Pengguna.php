<?php
    class Pengguna extends Controller
    {
        public function index()
        {
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $this->view('home/admin/pengguna', $data);
        }
    }