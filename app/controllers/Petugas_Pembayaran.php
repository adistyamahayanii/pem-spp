<?php
    class Admin_Pembayaran extends Controller
    {
        public function index()
        {
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/petugas/data-pembayaran/pembayaran', $data);
        }

        public function prosesTambah()
        {
            if($this->model('Pembayaran_model')->createPembayaran($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pembayaran');
                exit;
            }
        }
        
        public function edit($id)
        {
            $data['pembayaran'] = $this->model('Pembayaran_model')->getPembayaranById($id);
            $this->view('home/admin/data-pembayaran/edit-pembayaran', $data);
        }

        public function prosesUpdate()
        {
            if($this->model('Pembayaran_model')->updatePembayaran($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pembayaran');
                exit;
            }
        }

        public function delete($id)
        {
            if($this->model('Pembayaran_model')->deletePembayaran($id) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pembayaran');
                exit;
            }
        }
    }