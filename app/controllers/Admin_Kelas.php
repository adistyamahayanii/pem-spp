<?php
    class Admin_Kelas extends Controller
    {
        public function index()
        {
        $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
        $this->view('home/admin/data-kelas/kelas', $data);
        }

        public function prosesTambah()
        {
            if($this->model('Kelas_model')->createKelas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_kelas');
            }
        }

        public function edit($id)
        {
            $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
            $this->view('home/admin/data-kelas/edit-kelas', $data);
        }

        public function prosesUpdate($id)
        {
            // $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
            if($this->model('Kelas_model')->updateKelas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_kelas');
                exit;
            }
            
        }

        public function delete($id)
        {
            if($this->model('Kelas_model')->deleteKelas($id) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_kelas');
                exit;
            }
        }

        public function detail($id)
        {
            $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
            $this->view('home/admin/data-kelas/kelas', $data);
        }
    }