<?php

    class Admin extends Controller
    {
        public function index()
        {
            $this->view('home/admin/index');
        }

        public function petugas()
        {
            $data['petugas'] = $this->model('Petugas_model')->getAllPetugas();  
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $this->view('home/admin/data-petugas/petugas', $data);
        }

        public function pengguna()
        {
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $this->view('home/admin/data-pengguna/pengguna', $data);
        }

        public function siswa()
        {
            $data['siswa'] = $this->model('Siswa_model')->getAllSiswa();
            $this->view('home/admin/data-siswa/siswa', $data);
        }

        public function kelas()
        {
            $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
            $this->view('home/admin/data-kelas/kelas', $data);
        }

        public function pembayaran()
        {
            $data['pembayaran'] = $this->model('Pembayaran_model')->getAllPembayaran();
            $this->view('home/admin/data-pembayaran/pembayaran', $data);
        }

        public function transaksi()
        {
            $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
            $this->view('home/admin/data-transaksi/transaksi', $data);
        }

        public function history()
        {
            $this->view('home/admin/history');
        }
    }