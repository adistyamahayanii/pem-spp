<?php
    class Login extends Controller
    {
        public function index()
        {
            if(isset($_SESSION['pengguna']))
            {
                header('Location: ' . BASE_URL);
                exit;
            }
            $this->view('auth/login');
        }

        public function prosesLogin()
        {
            $dataUser = null;

            if (!$dataUser = $this->model('Siswa_model')->getSiswa($_POST['username'], $_POST['password'])) {
                if (!$dataUser = $this->model('Petugas_model')->getPetugas($_POST['username'], $_POST['password'])){
                Flasher::set('danger', 'Akun Tidak Terdaftar!');
                header('Location: ' . BASE_URL . '/login');
                exit;
                }
            }
            
            $_SESSION['username'] = $dataUser['username'];
            $_SESSION['is_logged_in'] = true;
    
            if ($dataUser['role'] == 'admin') {
                $_SESSION['role'] = 'admin';
                $_SESSION['petugas_id'] = $dataUser['petugas_id'];
                header('Location: ' . BASE_URL . '/admin');
                exit;
            } else if ($dataUser['role'] == 'petugas') {
                $_SESSION['role'] = 'petugas';
                $_SESSION['petugas_id'] = $dataUser['petugas_id'];
                header('Location: ' . BASE_URL . '/petugas');
                exit;
            } else {
                $_SESSION['role'] = 'siswa';
                $_SESSION['siswa_id'] = $dataUser['siswa_id'];
                header('Location: ' . BASE_URL . '/siswa');
                exit;
            }
        }
    
    }