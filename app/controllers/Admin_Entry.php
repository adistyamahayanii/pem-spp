<?php
    class Admin_Entry extends Controller
    {
        public function index()
        {
            $data = [
                'title' => 'Transaksi',
                'siswa' => $this->model('Siswa_model')->allSiswa()
            ];

            $this->view('home/admin/entry-pembayaran/index', $data);
        }

        public function showTransaksi($id)
        {
            $data = [
                'title' => 'Transaksi Siswa',
                'siswa' => $this->model('Siswa_model')->getDataById($id),
                'bulan' => $this->model('Transaksi_model')->getBulanByIdTransaksiSiswa($id)
            ];

            $data['dataBulan'] = [
                'Januari' => [
                    'Januari', 1
                ],
                'Februari' => [
                    'Februari', 2
                ],
                'Maret' => [
                    'Maret', 3
                ],
                'April' => [
                    'April', 4
                ],
                'Mei' => [
                    'Mei', 5
                ],
                'Juni' => [
                    'Juni', 6
                ],
                'Juli' => [
                    'Juli', 7
                ],
                'Agustus' => [
                    'Agustus', 8
                ],
                'September' => [
                    'September', 9
                ],
                'Oktober' => [
                    'Oktober', 10
                ],
                'November' => [
                    'November', 11
                ],
                'Desember' => [
                    'Desember', 12
                ],
            ];

            $bulan_dibayar = [];

            foreach ($data['bulan'] as $bulan) {
                array_push($bulan_dibayar, $bulan['bulan_dibayar']);
            }

            $data['bulan_dibayar'] = $bulan_dibayar;

            $this->view('home/admin/entry-pembayaran/showTransaksi', $data);
        }

        public function storeTransaksi()
        {
            if ($this->model('Transaksi_model')->addTransaksi($_POST) > 0) {
                Flasher::set('success', 'Data Transaksi Berhasil Ditambah');
                header('Location: ' . BASE_URL . '/admin_entry');
            }
        }
    }