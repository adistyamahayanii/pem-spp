<?php
    class Home extends Controller
    {
        public function index()
        {
            $this->view('home/admin/index');
        }

        public function petugas()
        {
            $this->view('home/petugas/index');
        }

        public function siswa()
        {
            $this->view('home/siswa/index');
        }
    }