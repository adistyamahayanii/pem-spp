<?php
    class Admin_History extends Controller
    {
        public function index()
        {
            $data['siswa'] = $this->model('Siswa_model')->allSiswa();
            return $this->view('home/admin/history/index', $data);
        }

        public function historySiswa($id)
        {
            $data['transaksi'] = $this->model('Transaksi_model')->getTransaksiByIdSiswa($id);
            return $this->view('home/admin/history/show', $data);
        }
    }