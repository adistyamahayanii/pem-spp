<?php
    class Siswa_Transaksi extends Controller 
    {
        public function index()
        {
            $data['transaksi'] = $this->model('Transaksi_model')->getAllTransaksi();
            $this->view('home/siswa/data-transaksi/transaksi', $data);
        }
    }