<?php
    class Petugas_Kelas extends Controller
    {
        public function index()
        {
            $data['kelas'] = $this->model('Kelas_model')->getAllKelas();
            $this->view('home/petugas/data-kelas/kelas', $data);
        }

        public function prosesTambah()
        {
            if($this->model('Kelas_model')->createKelas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/petugas_kelas');
                exit;
            }
        }

        public function edit($id)
        {
            $data['kelas'] = $this->model('Kelas_model')->getKelasById($id);
            $this->view('home/petugas/data-kelas/edit-kelas', $data);
        }

        public function prosesUpdate()
        {
            if($this->model('Kelas_model')->updateKelas($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/petugas_kelas');
                exit;
            }

        }

        public function delete($id)
        {
            if($this->model('Kelas_model')->deleteKelas($id) > 0)
            {
                header('Location: ' . BASE_URL . '/petugas_kelas');
                exit;
            }
        }
    }