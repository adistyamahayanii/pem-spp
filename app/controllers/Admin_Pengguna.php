<?php
    class Admin_Pengguna extends Controller
    {
        public function index()
        {
            $data['pengguna'] = $this->model('Pengguna_model')->getAllPengguna();
            $this->view('home/admin/data-pengguna/pengguna', $data);
        }

        public function prosesTambah()
        {
            if($this->model('Pengguna_model')->createPengguna($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pengguna');
                exit;
            }
        }

        public function edit($id)
        {
            $data['pengguna'] = $this->model('Pengguna_model')->getPenggunaById($id);
            $this->view('home/admin/data-pengguna/edit-pengguna', $data);
        }

        public function prosesUpdate()
        {
            if($this->model('Pengguna_model')->updatePengguna($_POST) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pengguna');
                exit;
            }
        }

        public function delete($id)
        {
            if($this->model('Pengguna_model')->deletePengguna($id) > 0)
            {
                header('Location: ' . BASE_URL . '/admin_pengguna');
                exit;
            }
        }
    }