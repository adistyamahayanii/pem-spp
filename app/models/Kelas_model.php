<?php
    class Kelas_model
    {
        private $table = 'kelas';
        private $db;

        public function __construct()
        {
            $this->db = new Database;
        }

        public function getAllKelas()
        {
            $query = "CALL getAllKelas";
            $this->db->query($query);
            return $this->db->resultAll();
        }

        public function getKelasById($id)
        {
            $query = 'SELECT * FROM ' . $this->table . ' WHERE id = :id';
            $this->db->query($query);
            $this->db->bind(':id', $id);
            return $this->db->resultSingle();
        }

        public function createKelas($data)
        {
            $query = 'call insertDataKelas(:nama, :kompetensi_keahlian)';
            $this->db->query($query);
            $this->db->bind('nama', $data['nama']);
            $this->db->bind('kompetensi_keahlian', $data['kompetensi_keahlian']);
            // $this->db->execute();
            return $this->db->rowCount();
        }

        public function updateKelas($data)
        {
            $query = 'call updateDataKelas(:id, :nama, :kompetensi_keahlian)';
            $this->db->query($query);
            $this->db->bind('id', $data['id']);
            $this->db->bind('nama', $data['nama']);
            $this->db->bind('kompetensi_keahlian', $data['kompetensi_keahlian']);
            return $this->db->rowCount();
        }

        public function deleteKelas($id)
        {
            $query = 'call deleteDataKelas(:id)';
            $this->db->query($query);
            $this->db->bind('id', $id);
            return $this->db->rowCount();
        }



    }