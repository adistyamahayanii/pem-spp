<?php 

    class App {

        protected $controller = 'Login';
        protected $method = 'index';
        protected $params = [];

        public function __construct()
        {
            $url = $this->parseURL();

            //controller
            if (isset($url[0])) // pertama, cek ada url[0] ga?
            {
                if  (file_exists('../app/controllers/' . $url[0] . '.php'))
                {
                    $this->controller = $url[0];
                    unset($url[0]);
                }
            }
            require_once "../app/controllers/" . $this->controller . ".php";
            $this->controller = new $this->controller;

            //method
            if (isset($url[1])) 
            {
                if (method_exists($this->controller, $url[1]))
                {
                    $this->method = $url[1];
                    unset($url[1]);
                }
            }

            //params
            if (!empty($url))
            {
                $this->params = array_values($url);
            }

            //jalankan controller & method serta kirim params, jika ada
            call_user_func_array([$this->controller, $this->method], $this->params);
        }

        public function parseURL()
        {
            if ( isset($_GET["url"]) ) { //fungsi isset memeriksa apakah parameter "url" diatur dalam baris $_GET
                $url = rtrim($_GET["url"], "/"); //menghapus semua garis miring dari variabel $url
                $url = filter_var($url, FILTER_SANITIZE_URL); //filter_var() digunakan untuk memfilter dan membersihkan variabel $url menggunakan filter FILTER_SANITIZE_URL (ini menghapus semua karakter URL ilegal dari string)
                $url = explode("/", $url); //membagi variabel $url yang disanitasi menjadi sebuah array, menggunakan karakter "/" sebagai pembatas
                return $url;
            }
        }
    }