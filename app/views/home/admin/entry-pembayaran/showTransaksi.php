<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

<div class="row m-4">
    <div class="col-xl-4 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-l font-weight-bold text-primary text-uppercase mb-1">
                            <?= $data['siswa']['nama'] ?></div>
                        <div class="h6 mb-1 font-weight-bold text-gray-800">Kelas : <?= $data['siswa']['nama_kelas'] ?></div>
                        <div class="h6 mb-1 font-weight-bold text-gray-800">Tahun Ajaran : <?= $data['siswa']['tahun_ajaran'] ?></div>
                        <div class="h6 mb-0 font-weight-bold text-gray-800">Nominal : <?= $data['siswa']['nominal'] ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-4">
    <div class="col-12">
        <form action="<?= BASE_URL ?>/admin_entry/storeTransaksi" class="d-inline" method="POST">
            <input type="hidden" name="siswa_id" value="<?= $data['siswa']['id'] ?>">
            <input type="hidden" name="pembayaran_id" value="<?= $data['siswa']['pembayaran_id'] ?>">
            <div class="row">
                <?php foreach ($data['dataBulan'] as $bulan) : ?>
                    <div class="col-4">
                        <div class="input-group mb-2 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <input type="checkbox" name="bulan_dibayar[]" id="" value="<?= $bulan[1] ?>" <?= in_array($bulan[1], $data['bulan_dibayar']) ? "checked disabled" : "   " ?> />
                                </div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="<?= $bulan[0] ?>" disabled>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <button type="submit" class="btn btn-primary mt-2">Submit</button>
        </form>
    </div>
</div>



<?php require_once TEMPLATE_PATH . '/footer.php'?>

            