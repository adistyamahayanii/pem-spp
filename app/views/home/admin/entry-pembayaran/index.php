<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

                

                <!-- Begin Page Content -->
                <div class="container-fluid" style="text-align: center;">

                    <!-- Content Row -->
                    <div class="row">
                        
                    <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Siswa</h6>
                        </div>
                        <div class="card-body" style="text-align: center;">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>NISN</th>
                                            <th>NIS</th>
                                            <th>Nama</th>
                                            <th>Kelas</th>
                                            <th>Kompetensi Keahlian</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($data['siswa'] as $siswa):?>
                                        <tr>
                                            <td style="text-align: center;"><?=$siswa['nisn']?></td>
                                            <td style="text-align: center;"><?=$siswa['nis']?></td>
                                            <td style="text-align: left;"><?=$siswa['nama']?></td>
                                            <td style="text-align: left;"><?=$siswa['nama_kelas']?></td>
                                            <td style="text-align: left;"><?=$siswa['kompetensi_keahlian']?></td>
                                            <td>
                                            <a href="<?=BASE_URL?>/admin_entry/showTransaksi/<?=$siswa['id']?>" class="btn btn-primary">Bayar</a>
                                            </td>
                                        </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

<?php require_once TEMPLATE_PATH . '/footer.php'?>

            