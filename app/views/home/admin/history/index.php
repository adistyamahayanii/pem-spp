<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">History Pembayaran SPP</h1>
        <a href="<?=BASE_URL; ?>/admin_transaksi/lihatLaporan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

        <!-- Content Row -->
        <div class="row">
                        
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Data History Pembayaran Siswa</h6>
                    </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>NISN</th>
                                            <th>NIS</th>
                                            <th>Nama</th>
                                            <th>Aksi</th>
                                            <!-- <th>Siswa ID</th>
                                            <th>Petugas ID</th>
                                            <th>Pembayaran ID</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($data['siswa'] as $siswa):?>
                                        <tr>
                                            <td style="text-align: left;"><?=$siswa['nisn']?></td>
                                            <td style="text-align: left;"><?=$siswa['nis']?></td>
                                            <td style="text-align: left;"><?=$siswa['nama']?></td>
                                            <td>
                                                <a href="<?= BASE_URL ?>/admin_history/historySiswa/<?= $siswa['id'] ?>" class="btn btn-primary btn-xs">Lihat</a>
                                            </td>
                                        </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

</div>
<!-- /.container-fluid -->
<?php require_once TEMPLATE_PATH . '/footer.php' ?>