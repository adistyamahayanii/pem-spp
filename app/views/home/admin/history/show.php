<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

<!-- Begin Page Content -->
<div class="container-fluid" style="text-align: center;">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h4 class="h3 mb-0 text-gray-800">History Pembayaran, <b><?= $data['transaksi'][0]['nama'] ?></b></h4>
        <a href="<?=BASE_URL; ?>/admin_transaksi/lihatLaporan" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

        <!-- Content Row -->
        <div class="row">
                        
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Data History Pembayaran Siswa</h6>
                    </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Tanggal Bayar</th>
                                            <th>Tahun Dibayar</th>
                                            <th>Bulan Dibayar</th>
                                            <th>Tahun Ajaran</th>
                                            <th>Nominal</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach($data['transaksi'] as $transaksi) : ?>
                                    <tr>
                                        <td><?= $transaksi['tanggal_bayar'] ?></td>
                                        <td><?= $transaksi['tahun_dibayar'] ?></td>
                                        <td><?= $transaksi['bulan_dibayar'] ?></td>
                                        <td><?= $transaksi['tahun_ajaran'] ?></td>
                                        <td><?= $transaksi['nominal'] ?></td>
                                        <td><span class="badge badge-success" style="text-align: center;">Lunas</span></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

</div>
<?php require_once TEMPLATE_PATH . '/footer.php' ?>
