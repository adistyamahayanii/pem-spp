<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Content Row -->
        <div class="row">
                        
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary" style="text-align: center;">Tambah Data Kelas :</h6>
                        </div>
                        <div class="card-body">
                            <form class="user" method="POST" action="<?=BASE_URL?>/admin_kelas/prosesTambah">
                            <div class="form-group">
                                <input type="text" class="form-control"
                                        id="nama" name="nama" aria-describedby="emailHelp"
                                        placeholder="Masukkan Nama..">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control"
                                        id="kompetensi_keahlian" name="kompetensi_keahlian" placeholder="Masukkan Kompetensi Keahlian..">
                            </div>

                                <div class="form-footer">
                                <a href="<?=BASE_URL?>/admin_kelas" type="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
                                <button type="submit" class="btn btn-primary">Simpan Data</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



<?php require_once TEMPLATE_PATH . '/footer.php'?>

            