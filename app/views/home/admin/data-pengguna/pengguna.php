<?php require_once TEMPLATE_PATH . '/header/admin.php' ?>

                

                <!-- Begin Page Content -->
                <div class="container-fluid" style="text-align: center;">

                    <!-- Content Row -->
                    <div class="row">
                        
                    <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Pengguna</h6>
                        </div>
                        <div class="card-body">
                        <div class="" style="text-align: left;">
                        <button type="button" class="btn btn-success" style="margin-bottom: 20px;" data-toggle="modal" data-target="#createPengguna">
                            Tambah Data
                        </button>
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Username</th>
                                            <th>Role</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($data['pengguna'] as $pengguna):?>
                                        <tr>
                                            <td><?=$pengguna['id']?></td>
                                            <td><?=$pengguna['username']?></td>
                                            <td><?=$pengguna['role']?></td>
                                            <td>
                                            <a href="<?=BASE_URL?>/admin_pengguna/edit/<?=$pengguna['id']?>" class="btn btn-primary">Edit</a>
                                            <a href="<?=BASE_URL?>/admin_pengguna/delete/<?=$pengguna['id']?>" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus
                                            </td>
                                        </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>


                    </div>

                </div>
                <!-- /.container-fluid -->

                <!-- Modal Tambah  Data -->
                <div class="modal fade" id="createPengguna" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                <form class="user" method="POST" action="<?=BASE_URL;?>/admin_pengguna/prosesTambah">

                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Detail Data Siswa :</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control"
                                id="username" name="username" aria-describedby="emailHelp"
                                placeholder="Masukkan Username..">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control"
                                id="password" name="password" aria-describedby="emailHelp"
                                placeholder="Masukkan Password..">
                    </div>
                    <div class="form-group">
                        <label for="example">Pengguna ID</label>
                        <select name="role" class="form-control">
                            <?php foreach($data['pengguna'] as $pengguna):?>
                            <option value="<?=$pengguna['id']?>"><?=$pengguna['role']?></option>
                            <?php endforeach?>
                        </select>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan Data</button>
                    </div>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


<?php require_once TEMPLATE_PATH . '/footer.php'?>

            